return function(use)
  use { -- LSP Configuration & Plugins
    'neovim/nvim-lspconfig',
    requires = {
      -- Automatically install LSPs to stdpath for neovim
      'williamboman/mason.nvim',
      'williamboman/mason-lspconfig.nvim',

      -- Useful status updates for LSP
      'j-hui/fidget.nvim',

      -- Additional lua configuration, makes nvim stuff amazing
      'folke/neodev.nvim',
    },
  }

  use { -- Autocompletion
    'hrsh7th/nvim-cmp',
    requires = { 'hrsh7th/cmp-nvim-lsp', 'L3MON4D3/LuaSnip', 'saadparwaiz1/cmp_luasnip' },
  }

  use { -- Highlight, edit, and navigate code
    'nvim-treesitter/nvim-treesitter',
    run = function()
      pcall(require('nvim-treesitter.install').update { with_sync = true })
    end,
  }

  use { -- Additional text objects via treesitter
    'nvim-treesitter/nvim-treesitter-textobjects',
    after = 'nvim-treesitter',
  }

  -- Git related plugins
  use 'tpope/vim-fugitive'
  use 'tpope/vim-rhubarb'
  use 'lewis6991/gitsigns.nvim'

  use 'navarasu/onedark.nvim' -- Theme inspired by Atom

  use 'ellisonleao/gruvbox.nvim' -- Nvim port for the Gruvbox theme
  require('custom.plugin.gruvbox')

  use 'nvim-lualine/lualine.nvim' -- Fancier statusline

  use 'lukas-reineke/indent-blankline.nvim' -- Add indentation guides even on blank lines
  use 'numToStr/Comment.nvim' -- "gc" to comment visual regions/lines
  use 'tpope/vim-sleuth' -- Detect tabstop and shiftwidth automatically

  -- Fuzzy Finder (files, lsp, etc)
  use { 'nvim-telescope/telescope.nvim', branch = '0.1.x', requires = { 'nvim-lua/plenary.nvim' } }

  -- Fuzzy Finder Algorithm which requires local dependencies to be built. Only load if `make` is available
  use { 'nvim-telescope/telescope-fzf-native.nvim', run = 'make', cond = vim.fn.executable 'make' == 1 }

  -- File browser extension for telescope
  use 'nvim-telescope/telescope-file-browser.nvim'

  -- Automatically close tags
  use 'windwp/nvim-ts-autotag'

  -- Automatically close brackets
  use 'windwp/nvim-autopairs'

  -- Use nvim as a language server to inject diagnostics, code actions and more
  use 'jose-elias-alvarez/null-ls.nvim'

  -- Code formatter
  use 'MunifTanjim/prettier.nvim'

  -- Lightweight lsp plugin based on neovim's lsp with a highly performant UI
  use 'glepnir/lspsaga.nvim'

  -- Fork of vim-web-dev-icons for neovim
  use 'kyazdani42/nvim-web-devicons'

  -- A high performance color highlighter
  use 'norcalli/nvim-colorizer.lua'

  -- A snazzy bufferline
  use 'akinsho/nvim-bufferline.lua'

  -- Distraction-free mode
  use 'folke/zen-mode.nvim'

  -- Markdown live preview
  use 'iamcco/markdown-preview.nvim'
end

