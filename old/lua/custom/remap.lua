local keymap = vim.keymap
-- Keymaps for better default experience
-- See `:help vim.keymap.set()`
vim.keymap.set({ 'n', 'v' }, '<Space>', '<Nop>', { silent = true })
vim.keymap.set({ 'n', 'v' }, '<20>', '<Nop>', { silent = true })

-- você é um ぱうのく

-- Remap for dealing with word wrap
vim.keymap.set('n', 'k', "v:count == 0 ? 'gk' : 'k'", { expr = true, silent = true })
vim.keymap.set('n', 'j', "v:count == 0 ? 'gj' : 'j'", { expr = true, silent = true })

-- Remap for netrw
vim.keymap.set('n', '<leader>ef', vim.cmd.Ex, { silent = true })

-- Remap for opening terminal emulator
vim.keymap.set('n', '<leader>te', '<C-w>n:term <Return>i', { silent = true })

-- Tab-related
-- New tab
keymap.set('n', '<leader>tn', ':tabedit<Return>', { silent=true })
-- Close tab
keymap.set('n', '<leader>tq', ':tabclose<Return>', { silent=true })
-- Navigate between tabs
keymap.set('n', '<leader>th', ':tabprev<Return>', { silent=true })
keymap.set('n', '<leader>tl', ':tabnext<Return>', { silent=true })
-- Move tab
keymap.set('n', '<leader>tH', ':-tabmove<Return>', { silent=true })
keymap.set('n', '<leader>tL', ':+tabmove<Return>', { silent=true })

-- Window-related
-- New window
keymap.set('n', '<leader>wn', '<C-w>n', { silent = true })
-- Close window
keymap.set('n', '<leader>q', '<C-w>q', { silent = true })
-- Split window
keymap.set('n', '<leader>vs', ':split<Return><C-w>w', { silent = true })
keymap.set('n', '<leader>hs', ':vsplit<Return><C-w>w', { silent = true })
-- Navigate between windows
keymap.set('n', '<leader>wh', ':wincmd h<Return>')
keymap.set('n', '<leader>wj', ':wincmd j<Return>')
keymap.set('n', '<leader>wk', '<C-w>k')
keymap.set('n', '<leader>wl', ':wincmd l<Return>')

