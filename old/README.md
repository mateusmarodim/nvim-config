# Neovim Config
This repo contains the latest updates of my neovim setup. In the future I might change this to include my tmux config as well, but for now this is it.

## Plugins I use

## Credits and Thanks
The base for this setup is the [nvim-lua/kickstart-nvim](https://github.com/nvim-lua/kickstart.nvim) repo, which I split into several files for better organizing and fitting my personal needs. There you will find installation instructions, as well as some customization examples and suggestions. Also huge thanks to ThePrimeagen ( [github](https://github.com/ThePrimeagen/ThePrimeagen) | [youtube](https://www.youtube.com/ThePrimeagen) ) and TJ DeVries ( [github](https://github.com/tjdevries) | [youtube](https://www.youtube.com/@teej_dv/) ) for giving out lots of the inspiration I had to finally make this repo.
