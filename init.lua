require('myrolledjeans.base')
require('myrolledjeans.maps')
require('myrolledjeans.highlights')
require('myrolledjeans.plugins')

local has = function(x)
    return vim.fn.has(x) == 1
end

local is_linux = has "Linux"
local is_windows = has "win32"
local is_macos = has "macunix"

if is_linux then
    require("myrolledjeans.clipboard-linux")
end
if is_windows then
    require("myrolledjeans.clipboard-windows")
end
if is_macos then
    return
end
