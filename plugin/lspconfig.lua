local status, lspconfig = pcall(require, 'lspconfig')
if (not status) then return end

require 'myrolledjeans/cmp'

-- Options and general diagnostic mappings
local options = { noremap = true, silent = true }
vim.diagnostic.enable()
vim.keymap.set('n', '<leader>df', vim.diagnostic.open_float, options)
vim.keymap.set('n', '<leader>dn', vim.diagnostic.goto_next, options)
vim.keymap.set('n', '<leader>dp', vim.diagnostic.goto_prev, options)
vim.keymap.set('n', '<leader>ds', vim.diagnostic.setloclist, options)

local on_attach = function(client, bufnr)
    -- Enable completion triggered by <c-x><c-o>
    vim.api.nvim_buf_set_option(bufnr, 'omnifunc', 'v:lua.vim.lsp.omnifunc')

    -- LSP buffer-specific mappings
    local buffer_options = { noremap = true, silent = true, buffer = bufnr }
    local buffer_keymap = function(keys, func)
        vim.keymap.set('n', keys, func, buffer_options)
    end

    buffer_keymap('gD', vim.lsp.buf.declaration)
    buffer_keymap('gd', vim.lsp.buf.definition)
    buffer_keymap('gh', vim.lsp.buf.hover)
    buffer_keymap('gi', vim.lsp.buf.implementation)
    buffer_keymap('gs', vim.lsp.buf.signature_help)
    buffer_keymap('<leader>wa', vim.lsp.buf.add_workspace_folder)
    buffer_keymap('<leader>wr', vim.lsp.buf.remove_workspace_folder)
    buffer_keymap('<leader>wl', function()
        print(vim.inspect(vim.lsp.buf.list_workspace_folders()))
    end)
    buffer_keymap('td', vim.lsp.buf.type_definition)
    buffer_keymap('<leader>rn', vim.lsp.buf.rename)
    buffer_keymap('<leader>ca', vim.lsp.buf.code_action)
    buffer_keymap('gr', vim.lsp.buf.references)
    buffer_keymap('<leader>ff', function() vim.lsp.buf.format({ async = true }) end)
end

local lsp_flags = {
    debounce_text_changes = 150,
}

local capabilities = require('cmp_nvim_lsp').default_capabilities()

lspconfig.lua_ls.setup({
    on_attach = on_attach,
    flags = lsp_flags,
    capabilities = capabilities,
    settings = {
        Lua = {
            runtime = {
                version = 'LuaJIT',
            },
            diagnostics = {
                globals = { 'vim' },
            },
            workspace = {
                library = vim.api.nvim_get_runtime_file("", true),
            },
            telemetry = {
                enable = false,
            },
        },
    },
})
