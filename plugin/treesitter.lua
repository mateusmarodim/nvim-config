local status, treesitter_configs = pcall(require, 'nvim-treesitter.configs')
if not status then
    return
end

treesitter_configs.setup{
    ensure_installed = {
        'bash',
        'c',
        'cpp',
        'css',
        'help',
        'html',
        'java',
        'javascript',
        'json',
        'lua',
        'markdown',
        'python',
        'regex',
        'rust',
        'scss',
        'sql',
        'typescript',
        'tsx',
        'vim',
    },
    sync_install = true,
    auto_install = true,
    ignore_install = { "fish" },
    highlight = {
        enable = true,
        disable = function(lang, buf) -- Disable treesitter hightlighting for large files
            local max_filesize = 100 * 1024 -- 100 KB
            local ok, stats = pcall(vim.loop.fs_stat, vim.api.nvim_buf_get_name(buf))
            if ok and stats and stats.size > max_filesize then
                return true
            end
        end,
    }
}

local parser_config = require 'nvim-treesitter.parsers'.get_parser_configs()
parser_config.tsx.filetype_to_parsername = { 'javascript', 'typescript.tsx' }
