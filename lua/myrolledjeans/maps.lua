local keymap = vim.keymap
-- Packer-related keymaps
keymap.set('n', '<leader>pi', ':PackerInstall<Return>')
keymap.set('n', '<leader>ps', ':PackerSync<Return>')
-- Keymaps for better default experience
-- See `:help vim.keymap.set()`
keymap.set({ 'n', 'v' }, '<Space>', '<Nop>', { silent = true })
keymap.set({ 'n', 'v' }, '<20>', '<Nop>', { silent = true })

-- Remap for dealing with word wrap
keymap.set('n', 'k', "v:count == 0 ? 'gk' : 'k'", { expr = true, silent = true })
keymap.set('n', 'j', "v:count == 0 ? 'gj' : 'j'", { expr = true, silent = true })

-- Remap for netrw
keymap.set('n', '<leader>ef', vim.cmd.Ex, { silent = true })

-- Remap for opening terminal emulator
keymap.set('n', '<leader>te', '<C-w>n:term <Return>i', { silent = true })

-- Tab-related
-- New tab
keymap.set('n', '<leader>tn', ':tabedit<Return>', { silent = true })
-- Close tab
keymap.set('n', '<leader>tq', ':tabclose<Return>', { silent = true })
-- Navigate between tabs
keymap.set('n', '<leader>th', ':tabprev<Return>', { silent = true })
keymap.set('n', '<leader>tl', ':tabnext<Return>', { silent = true })
-- Move tab
keymap.set('n', '<leader>tH', ':-tabmove<Return>', { silent = true })
keymap.set('n', '<leader>tL', ':+tabmove<Return>', { silent = true })

-- Window-related
-- New window
keymap.set('n', '<leader>wn', '<C-w>n', { silent = true })
-- Close window
keymap.set('n', '<leader>q', '<C-w>q', { silent = true })
-- Close without saving
keymap.set('n', '<leader>Q', ':q!<Enter>', { silent = true })
-- Write and close window
keymap.set('n', '<leader>wq', ':wq<Enter>', { silent = true })
-- Just write buffer in current window
keymap.set('n', '<leader>ww', ':w<Enter>', { silent = true })
-- Write buffer in current window and exit to netrw
keymap.set('n', '<leader>we', ':w<Enter>:Ex<enter>', { silent = true })
-- Split window
keymap.set('n', '<leader>vs', ':split<Return><C-w>w', { silent = true })
keymap.set('n', '<leader>hs', ':vsplit<Return><C-w>w', { silent = true })
-- Navigate between windows
keymap.set('n', '<leader>wh', ':wincmd h<Return>', { silent = true })
keymap.set('n', '<leader>wj', ':wincmd j<Return>', { silent = true })
keymap.set('n', '<leader>wk', ':wincmd k<Return>', { silent = true })
keymap.set('n', '<leader>wl', ':wincmd l<Return>', { silent = true })
-- Move windows
keymap.set('n', '<leader>wH', ':wincmd H<Return>', { silent = true })
keymap.set('n', '<leader>wJ', ':wincmd J<Return>', { silent = true })
keymap.set('n', '<leader>wK', ':wincmd K<Return>', { silent = true })
keymap.set('n', '<leader>wL', ':wincmd L<Return>', { silent = true })
-- Resize windows
keymap.set('n', '<leader><S-h>', ':wincmd <<Return>', { silent = true })
keymap.set('n', '<leader><S-j>', ':wincmd -<Return>', { silent = true })
keymap.set('n', '<leader><S-k>', ':wincmd +<Return>', { silent = true })
keymap.set('n', '<leader><S-l>', ':wincmd ><Return>', { silent = true })

-- Do not yank with x
keymap.set('n', 'x', '"_x', { silent = true })

-- Increment/decrement
keymap.set('n', '+', '<C-a>', { silent = true })
keymap.set('n', '-', '<C-x>', { silent = true })

-- Delete a word backwards
-- keymap.set('n', 'dw', 'vb"_d', { silent = true })

-- Select all
keymap.set('n', '<C-a>', 'gg<S-v>G', { silent = true })

